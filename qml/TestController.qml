import QtQuick 2.0
import QtQuick.Controls 2.2

import principaltests 1.0

import "tests/"

Item {
    id: testController

    property TestControllerLogic logic

    Rectangle {
        id: background
        anchors.fill: parent
        color: "lightblue"
    }

    Button {
        id: startButton

        anchors.centerIn: parent

        text: "START TEST"

        onClicked: {
            logic.startExam()
            enabled = false
        }
    }

    SimpleReactionTest {
        anchors.fill: parent
        logic: _logic.simpleReactionTest
    }

    ChoiceReactionTest {
        anchors.fill: parent
        logic: _logic.choiceReactionTest
    }

    ClassNumberTest {
        anchors.fill: parent
        logic: _logic.classNumberTest
    }

    TwoBackTest {
        anchors.fill: parent
        logic: _logic.twoBackTest
    }
}
