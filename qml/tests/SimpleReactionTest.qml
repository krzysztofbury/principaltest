import QtQuick 2.0
import QtQuick.Controls 2.2

import principaltests 1.0

Item {
    id: simpleReactionTest

    property SimpleReactionTestLogic logic

    visible: logic.testIsActive
    focus: visible

    Rectangle {
        anchors.fill: parent
        color: "lightblue"
    }

    Rectangle {
        id: square

        width: 50
        height: 50
        anchors.centerIn: parent
        visible: logic.squareVisible

        color: "red"
        border.color: "black"
        border.width: 2
    }

    Button {
        width: parent.width - 10
        anchors {
            horizontalCenter: parent.horizontalCenter
            bottom: parent.bottom
            bottomMargin: 5
        }

        text: "GOT IT (can press spacebar)"
        onPressed: {
            logic.gotItClicked()
        }

        Keys.forwardTo: simpleReactionTest
    }

    Keys.onSpacePressed: {
        logic.gotItClicked()
    }

    Connections {
        target: logic
        onResetView: {}
    }
}
