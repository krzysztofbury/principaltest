import QtQuick 2.0
import QtQuick.Controls 2.2

import principaltests 1.0

Item {
    id: twoBackTest

    property TwoBackTestLogic logic

    visible: logic.testIsActive

    Rectangle {
        anchors.fill: parent
        color: "lightblue"
    }

    Connections {
        target: logic
        onResetView: {
        }
    }
}
