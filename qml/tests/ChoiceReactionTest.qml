import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2
import QtQuick 2.0

import principaltests 1.0

Item {
    id: choiceReactionTestLogic

    property ChoiceReactionTestLogic logic

    visible: logic.testIsActive

    Rectangle {
        anchors.fill: parent
        color: "lightblue"
    }

    Rectangle {
        id: square

        width: 50
        height: 50
        anchors.centerIn: parent
        //        visible: logic.squareVisible

        color: "red"
        border.color: "black"
        border.width: 2
    }

    Rectangle {
        id: circle

        width: 50
        height: 50
        anchors.centerIn: parent
        //        visible: logic.squareVisible
        radius: width/2

        color: "blue"
        border.color: "black"
        border.width: 2
    }

    RowLayout {
        id: buttons

        spacing: 5

        width: parent.width - 10
        anchors {
            horizontalCenter: parent.horizontalCenter
            bottom: parent.bottom
            bottomMargin: 5
        }

        Button {
            Layout.fillWidth: true

            text: "SQUARE (can press LEFT ARROW)"
            onPressed: {
                //                logic.gotItClicked()
            }

            Keys.forwardTo: choiceReactionTestLogic
        }

        Button {
            Layout.fillWidth: true

            text: "CIRCLE (can press RIGHT ARROW)"
            onPressed: {
                //                logic.gotItClicked()
            }

            Keys.forwardTo: choiceReactionTestLogic
        }
    }


    Keys.onLeftPressed: {
        print("l")
    }
    Keys.onRightPressed: {
        print("r")
    }

    Connections {
        target: logic
        onResetView: {
        }
    }
}
/*




    Connections {
        target: logic
        onResetView: {}
    }
}
*/
