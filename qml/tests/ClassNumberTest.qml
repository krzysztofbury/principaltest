import QtQuick 2.0
import QtQuick.Controls 2.2

import principaltests 1.0

Item {
    id: classNumberTest

    property ClassNumberTestLogic logic

    visible: logic.testIsActive

    Rectangle {
        anchors.fill: parent
        color: "lightblue"
    }

    Column {
        anchors.centerIn: parent
        spacing: 5

        Text {
            text: "Klasa :"
            font.pointSize: 10
        }
        TextField {
            id: klasa
        }
        Text {
            text: "Nr. w dzienniku :"
            font.pointSize: 10
        }
        TextField {
            id: numer
        }
        Button {
            text: "Dalej"
            onClicked: {
                if (klasa.text.length > 0 && numer.text.length > 0) {
                    logic.setClassAndName(klasa.text, numer.text);
                }
            }
        }
    }

    Connections {
        target: logic
        onResetView: {
            klasa.text = ""
            numer.text = ""
        }
    }
}
