#ifndef LOCALSOURCE_H
#define LOCALSOURCE_H

#include <QObject>

class LocalSource : public QObject
{
    Q_OBJECT
public:
    explicit LocalSource(QObject *parent = nullptr);

    void initData();

signals:
    void configLoaded(QString configJson);
};

#endif // LOCALSOURCE_H
