#include "principaltestsapplication.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    PrincipalTestsApplication principalTestsApplication(argc, argv);

    return principalTestsApplication.exec();
}
