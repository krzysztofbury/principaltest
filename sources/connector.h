#ifndef CONNECTOR_H
#define CONNECTOR_H

#include <QObject>

#include "features/testcontrollerlogic.h"
#include "dataSources/localsource.h"
#include "managers/managers.h"

class Connector : public QObject
{
    Q_OBJECT

public:
    explicit Connector(QObject* parent = nullptr);

    void init() const;

    TestControllerLogic* mainLogic();

private:
    TestControllerLogic* mTestControllerLogic;
    LocalSource* mLocalSource;
    Managers* mManagers;

    void connectDataSources() const;
    void connectManagers() const;
    void connectFeatures() const;
};

#endif // CONNECTOR_H
