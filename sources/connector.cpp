#include "connector.h"

Connector::Connector(QObject *parent)
    : QObject(parent)
    , mTestControllerLogic(new TestControllerLogic(this))
    , mLocalSource(new LocalSource(this))
    , mManagers(new Managers(this))
{
}

void Connector::init() const
{
    connectDataSources();
    connectManagers();
    connectFeatures();

    mLocalSource->initData();
}

TestControllerLogic* Connector::mainLogic()
{
    return mTestControllerLogic;
}

PRIVATE_METHODS
void Connector::connectDataSources() const
{
    connect(mLocalSource, SIGNAL(configLoaded(QString)), mManagers->config(), SLOT(onConfigLoaded(QString)));
}

void Connector::connectManagers() const
{
    connect(mManagers->config(), SIGNAL(configUpdated(QVector<QMap<QString,QString>>)), mTestControllerLogic, SLOT(onConfigUpdated(QVector<QMap<QString,QString>>)));
}

void Connector::connectFeatures() const
{
    connect(mTestControllerLogic->simpleReactionTest(), SIGNAL(sendResult(QString,QMap<QString,QString>)), mManagers->results(), SLOT(addResult(QString,QMap<QString,QString>)));
    connect(mTestControllerLogic->choiceReactionTest(), SIGNAL(sendResult(QString,QMap<QString,QString>)), mManagers->results(), SLOT(addResult(QString,QMap<QString,QString>)));
    connect(mTestControllerLogic->classNumberTest(), SIGNAL(sendResult(QString,QMap<QString,QString>)), mManagers->results(), SLOT(addResult(QString,QMap<QString,QString>)));
    connect(mTestControllerLogic->twoBackTest(), SIGNAL(sendResult(QString,QMap<QString,QString>)), mManagers->results(), SLOT(addResult(QString,QMap<QString,QString>)));
    connect(mTestControllerLogic->simpleReactionTest(), SIGNAL(testCompleted()), mTestControllerLogic, SLOT(onTestCompleted()));
    connect(mTestControllerLogic->choiceReactionTest(), SIGNAL(testCompleted()), mTestControllerLogic, SLOT(onTestCompleted()));
    connect(mTestControllerLogic->classNumberTest(), SIGNAL(testCompleted()), mTestControllerLogic, SLOT(onTestCompleted()));
    connect(mTestControllerLogic->twoBackTest(), SIGNAL(testCompleted()), mTestControllerLogic, SLOT(onTestCompleted()));
}
