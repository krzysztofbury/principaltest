#include <QQmlFileSelector>
#include <QFileSelector>
#include <QQmlContext>

#include "principaltestsapplication.h"

PrincipalTestsApplication::PrincipalTestsApplication(int &argc, char **argv)
    : QGuiApplication(argc, argv)
    , mQmlApplicationEngine(nullptr)
    , mConnector(nullptr)
{
    mQmlApplicationEngine = new QQmlApplicationEngine();
    mConnector = new Connector();

    mQmlApplicationEngine->rootContext()->setContextProperty("_logic", mConnector->mainLogic());

    registerAllQmlModelTypes();
    registerAllMetaTypes();

    mConnector->init();

    showQml();
}

PrincipalTestsApplication::~PrincipalTestsApplication()
{
    delete mQmlApplicationEngine;
    delete mConnector;
}

PRIVATE_METHODS
void PrincipalTestsApplication::registerAllQmlModelTypes() const
{
    QML_REGISTER_UNCREATABLE_TYPE(SimpleReactionTestLogic)
    QML_REGISTER_UNCREATABLE_TYPE(ChoiceReactionTestLogic)
    QML_REGISTER_UNCREATABLE_TYPE(ClassNumberTestLogic)
    QML_REGISTER_UNCREATABLE_TYPE(TestControllerLogic)
    QML_REGISTER_UNCREATABLE_TYPE(TwoBackTestLogic)
}

void PrincipalTestsApplication::registerAllMetaTypes() const
{
//qRegisterMetaType<QVector<...Struct>>("QVector<...Struct>");
}

void PrincipalTestsApplication::showQml() const
{
    mQmlApplicationEngine->load(QUrl(QStringLiteral("qrc:/main.qml")));
}
