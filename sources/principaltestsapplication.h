#ifndef PRINCIPALTESTSAPPLICATION_H
#define PRINCIPALTESTSAPPLICATION_H

#include <QQmlApplicationEngine>
#include <QGuiApplication>

#include "connector.h"
#include "macros.h"

class PrincipalTestsApplication : public QGuiApplication
{
public:
    explicit PrincipalTestsApplication(int& argc, char** argv);
    ~PrincipalTestsApplication();

private:
    void registerAllQmlModelTypes() const;
    void registerAllMetaTypes() const;
    void showQml() const;

    QQmlApplicationEngine* mQmlApplicationEngine;
    Connector* mConnector;
};

#endif // PRINCIPALTESTSAPPLICATION_H
