#ifndef MACROS_H
#define MACROS_H

#include <QQmlEngine>

#define QML_REGISTER_UNCREATABLE_TYPE(Type) qmlRegisterUncreatableType<Type>("principaltests", 1, 0, #Type, "C++ only!");
#define QML_CALL // empty method marker that this signal/slot is called directly from QML
#define QML_SLOT // empty method marker that slot for the signal is in QML

// Empty method markers for .cpp files
#define STATIC_PUBLIC_METHODS
#define Q_INVOKABLE_METHODS
#define PROTECTED_METHODS
#define PRIVATE_METHODS
#define PROTECTED_SLOTS
#define PUBLIC_METHODS
#define PRIVATE_SLOTS
#define PUBLIC_SLOTS

#define AUTO_Q_PROPERTY(Type, name) \
    protected: \
        Q_PROPERTY (Type name READ name WRITE set_##name NOTIFY name##Changed) \
    \
    private: \
        Type m_##name; \
    \
    public: \
        Type name() const \
        { \
            return m_##name; \
        } \
    \
    public Q_SLOTS: \
        void set_##name(Type name) \
        { \
            if (m_##name == name) \
            { \
                return; \
            } \
        \
            m_##name = name; \
            emit name##Changed(m_##name); \
        } \
    \
    Q_SIGNALS: \
        void name##Changed(Type name); \
    \
    private:

#endif // MACROS_H
