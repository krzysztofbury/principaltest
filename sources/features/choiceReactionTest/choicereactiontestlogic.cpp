#include <QDebug>

#include "choicereactiontestlogic.h"

ChoiceReactionTestLogic::ChoiceReactionTestLogic(QObject *parent)
    : BaseTest(parent)
    , m_choice1Visible(false)
    , m_choice2Visible(false)
    , m_numberOfTries(0)
    , mCurrentTry(0)
{
    mTestId = "choice_reaction_test";

    mTimer = new QTimer(this);
    mTimer->setSingleShot(true);
    connect(mTimer, SIGNAL(timeout()), SLOT(showChoice()));
}

void ChoiceReactionTestLogic::setTestConfig(const QMap<QString, QString> &config)
{
    qDebug() << "TEST SET CONFIG :" << config;
    emit resetView();
}

void ChoiceReactionTestLogic::startTest()
{
    qDebug() << "start Test";
    set_testIsActive(true);
}

Q_INVOKABLE_METHODS
void ChoiceReactionTestLogic::gotItClicked(int choiceNumber)
{

}

PRIVATE_METHODS
void ChoiceReactionTestLogic::startTry()
{

}

PRIVATE_SLOTS
void ChoiceReactionTestLogic::showChoice()
{

}
