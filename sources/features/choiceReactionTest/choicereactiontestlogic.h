#ifndef CHOICEREACTIONTESTLOGIC_H
#define CHOICEREACTIONTESTLOGIC_H

#include <QObject>
#include <QTimer>

#include "../basetest.h"

class ChoiceReactionTestLogic : public BaseTest
{
    Q_OBJECT

    AUTO_Q_PROPERTY(bool, choice1Visible)
    AUTO_Q_PROPERTY(bool, choice2Visible)
    AUTO_Q_PROPERTY(int, numberOfTries)

public:
    explicit ChoiceReactionTestLogic(QObject *parent = nullptr);

    void setTestConfig(const QMap<QString,QString>& config) override;
    void startTest() override;

    Q_INVOKABLE void gotItClicked(int choiceNumber);
private:
    int mDelaySecoundsAtLeast;
    int mDelaySecoundsMaximum;
    qint64 mTimestamp;
    int mCurrentTry;
    QTimer* mTimer;

    void startTry();

private slots:
    void showChoice();
};

#endif // CHOICEREACTIONTESTLOGIC_H
