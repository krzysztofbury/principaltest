#ifndef TESTCONTROLLERLOGIC_H
#define TESTCONTROLLERLOGIC_H

#include <QObject>
#include <QVector>

#include "simpleReactionTest/simplereactiontestlogic.h"
#include "choiceReactionTest/choicereactiontestlogic.h"
#include "classNumberTest/classnumbertestlogic.h"
#include "twoBackTest/twobacktestlogic.h"
#include "../macros.h"

class TestControllerLogic : public QObject
{
    Q_OBJECT

    AUTO_Q_PROPERTY(SimpleReactionTestLogic*, simpleReactionTest)
    AUTO_Q_PROPERTY(ChoiceReactionTestLogic*, choiceReactionTest)
    AUTO_Q_PROPERTY(ClassNumberTestLogic*, classNumberTest)
    AUTO_Q_PROPERTY(TwoBackTestLogic*, twoBackTest)

public:
    explicit TestControllerLogic(QObject *parent = nullptr);

    Q_INVOKABLE void startExam();

private:
    QVector<QMap<QString, QString>> mConfig;
    int mCurrentTest;

    void runTestWithConfig(const QMap<QString, QString>& testConfig);
    BaseTest* getTestForId(const QString& testId);

public slots:
    void onConfigUpdated(const QVector<QMap<QString, QString>>& config);
    void onTestCompleted();

signals:
    void examCompleted();
};

#endif // TESTCONTROLLERLOGIC_H
