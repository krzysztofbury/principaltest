#ifndef CLASSNUMBERTESTLOGIC_H
#define CLASSNUMBERTESTLOGIC_H

#include <QObject>
#include <QMap>

#include "../basetest.h"

class ClassNumberTestLogic : public BaseTest
{
    Q_OBJECT

public:
    explicit ClassNumberTestLogic(QObject *parent = nullptr);

    void setTestConfig(const QMap<QString,QString>& config) override;
    void startTest() override;

    Q_INVOKABLE void setClassAndName(QString userClass, QString userNumber);
};

#endif // CLASSNUMBERTESTLOGIC_H
