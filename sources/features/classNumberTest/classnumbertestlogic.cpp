#include <QDebug>

#include "classnumbertestlogic.h"

ClassNumberTestLogic::ClassNumberTestLogic(QObject *parent)
    : BaseTest(parent)
{
    mTestId = "class_number_test";
}

void ClassNumberTestLogic::setTestConfig(const QMap<QString,QString>& config)
{
    emit resetView();
}

void ClassNumberTestLogic::startTest()
{
    set_testIsActive(true);
}

void ClassNumberTestLogic::setClassAndName(QString userClass, QString userNumber)
{
    QMap<QString, QString> params;
    params["numer"] = userNumber;
    params["klasa"] = userClass;

    sendResult(testId(), params);

    set_testIsActive(false);
    emit testCompleted();
}
