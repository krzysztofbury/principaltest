#include "basetest.h"

BaseTest::BaseTest(QObject *parent)
    : QObject(parent)
    , m_testIsActive(false)
    , mTestId("")
{
}

void BaseTest::startTest()
{
}

QString BaseTest::testId()
{
    return mTestId;
}
