#ifndef BASETEST_H
#define BASETEST_H

#include <QObject>

#include "../macros.h"

class BaseTest : public QObject
{
    Q_OBJECT

    AUTO_Q_PROPERTY(bool, testIsActive)

public:
    explicit BaseTest(QObject *parent = nullptr);

    virtual void setTestConfig(const QMap<QString,QString>& config)=0;
    virtual void startTest();
    QString testId();

protected:
    QString mTestId;

signals:
    QML_CALL void resetView();
    void sendResult(const QString& testId, const QMap<QString,QString>& parameters);
    void testCompleted();
};

#endif // BASETEST_H
