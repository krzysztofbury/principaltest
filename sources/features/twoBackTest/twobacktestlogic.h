#ifndef TWOBACKTESTLOGIC_H
#define TWOBACKTESTLOGIC_H

#include <QObject>

#include "../basetest.h"

class TwoBackTestLogic : public BaseTest
{
    Q_OBJECT
public:
    explicit TwoBackTestLogic(QObject *parent = nullptr);

    void setTestConfig(const QMap<QString,QString>& config) override;
    void startTest() override;
};

#endif // TWOBACKTESTLOGIC_H
