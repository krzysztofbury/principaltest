#include <QDebug>

#include "twobacktestlogic.h"

TwoBackTestLogic::TwoBackTestLogic(QObject *parent)
    : BaseTest(parent)
{
    mTestId = "two_back_test";
}

void TwoBackTestLogic::setTestConfig(const QMap<QString, QString> &config)
{
    qDebug() << "TEST SET CONFIG :" << config;
    emit resetView();
}

void TwoBackTestLogic::startTest()
{
    set_testIsActive(true);
}
