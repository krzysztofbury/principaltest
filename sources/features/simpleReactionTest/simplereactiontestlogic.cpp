#include <QDateTime>
#include <QDebug>
#include <QTime>

#include "simplereactiontestlogic.h"

SimpleReactionTestLogic::SimpleReactionTestLogic(QObject *parent)
    : BaseTest(parent)
    , m_squareVisible(false)
    , m_numberOfTries(0)
    , mCurrentTry(0)
{   
    mTestId = "simple_reaction_test";

    mTimer = new QTimer(this);
    mTimer->setSingleShot(true);
    connect(mTimer, SIGNAL(timeout()), SLOT(showSquare()));
}

void SimpleReactionTestLogic::setTestConfig(const QMap<QString, QString> &config)
{
    set_numberOfTries(config["number_of_tries"].toInt());

    mDelaySecoundsAtLeast = config["delay_secounds_at_least"].toInt();
    mDelaySecoundsMaximum = config["delay_secounds_maximum"].toInt();

    set_squareVisible(false);
    mCurrentTry = 0;

    emit resetView();
}

void SimpleReactionTestLogic::startTest()
{
    set_testIsActive(true);
    startTry();
}

Q_INVOKABLE_METHODS
void SimpleReactionTestLogic::gotItClicked()
{
    QMap<QString, QString> params;

    if (!squareVisible()) {
        mTimer->stop();
        params["result"] = "FAIL";
        params["reaction_time"] = "0";
    } else {
        params["result"] = "SUCCESS";

        qint64 reactionTime = QDateTime::currentMSecsSinceEpoch() - mTimestamp;
        params["reaction_time"] = QString::number(reactionTime);
    }
    params["try_number"] = QString::number(mCurrentTry);

    sendResult(testId(), params);

    set_squareVisible(false);

    if (mCurrentTry == numberOfTries()) {
        set_testIsActive(false);
        emit testCompleted();
    } else {
        startTry();
    }
}

PRIVATE_METHODS
void SimpleReactionTestLogic::startTry()
{
    qsrand(static_cast<quint64>(QTime::currentTime().msecsSinceStartOfDay()));
    int milisecounds = qrand()%((mDelaySecoundsMaximum - mDelaySecoundsAtLeast)*1000 + 1) + mDelaySecoundsAtLeast*1000;

    mCurrentTry++;

    mTimer->setInterval(milisecounds);
    mTimer->start();
}

PRIVATE_SLOTS
void SimpleReactionTestLogic::showSquare()
{
    set_squareVisible(true);
    mTimestamp = QDateTime::currentMSecsSinceEpoch();
}
