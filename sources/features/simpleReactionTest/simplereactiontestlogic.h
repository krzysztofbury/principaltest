#ifndef SIMPLEREACTIONTESTLOGIC_H
#define SIMPLEREACTIONTESTLOGIC_H

#include <QObject>
#include <QTimer>

#include "../basetest.h"

class SimpleReactionTestLogic : public BaseTest
{
    Q_OBJECT

    AUTO_Q_PROPERTY(bool, squareVisible)
    AUTO_Q_PROPERTY(int, numberOfTries)

public:
    explicit SimpleReactionTestLogic(QObject *parent = nullptr);

    void setTestConfig(const QMap<QString,QString>& config) override;
    void startTest() override;

    Q_INVOKABLE void gotItClicked();
private:
    int mDelaySecoundsAtLeast;
    int mDelaySecoundsMaximum;
    qint64 mTimestamp;
    int mCurrentTry;
    QTimer* mTimer;

    void startTry();

private slots:
    void showSquare();
};

#endif // SIMPLEREACTIONTESTLOGIC_H
