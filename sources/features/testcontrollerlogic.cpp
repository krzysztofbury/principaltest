#include <QDebug>

#include "testcontrollerlogic.h"

TestControllerLogic::TestControllerLogic(QObject *parent)
    : QObject(parent)
    , m_simpleReactionTest(new SimpleReactionTestLogic(this))
    , m_choiceReactionTest(new ChoiceReactionTestLogic(this))
    , m_classNumberTest(new ClassNumberTestLogic(this))
    , m_twoBackTest(new TwoBackTestLogic(this))
{
}

Q_INVOKABLE_METHODS
void TestControllerLogic::startExam()
{
    mCurrentTest = 0;
    QMap<QString, QString> testConfig = mConfig[mCurrentTest];
    runTestWithConfig(testConfig);
}

void TestControllerLogic::runTestWithConfig(const QMap<QString, QString> &testConfig)
{
    QString testId = testConfig["test_id"];
    BaseTest* testToRun = getTestForId(testId);
    testToRun->setTestConfig(testConfig);
    testToRun->startTest();
}

BaseTest* TestControllerLogic::getTestForId(const QString &testId)
{
    if(simpleReactionTest()->testId() == testId) return simpleReactionTest();
    if(choiceReactionTest()->testId() == testId) return choiceReactionTest();
    if(classNumberTest()->testId() == testId) return classNumberTest();
    if(twoBackTest()->testId() == testId) return twoBackTest();

    qWarning() << "COULD NOT FIND TEST WITH ID:" << testId;
    return nullptr;
}


PUBLIC_SLOTS
void TestControllerLogic::onConfigUpdated(const QVector<QMap<QString, QString>>& config)
{
    mConfig = config;
}

void TestControllerLogic::onTestCompleted()
{
    mCurrentTest++;
    if (mConfig.length() > mCurrentTest) {
        QMap<QString, QString> testConfig = mConfig[mCurrentTest];
        runTestWithConfig(testConfig);
    } else {
        emit examCompleted();
    }
}
