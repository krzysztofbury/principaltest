#ifndef RESULTSMANAGER_H
#define RESULTSMANAGER_H

#include <QObject>

#include "../../macros.h"

class ResultsManager : public QObject
{
    Q_OBJECT

public:
    explicit ResultsManager(QObject *parent = nullptr);

public slots:
    void addResult(const QString& testId, const QMap<QString,QString>& parameters);

};

#endif // RESULTSMANAGER_H
