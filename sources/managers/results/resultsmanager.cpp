#include <QDateTime>
#include <QDebug>

#include "resultsmanager.h"

ResultsManager::ResultsManager(QObject *parent)
    : QObject(parent)
{
}

PUBLIC_SLOTS
void ResultsManager::addResult(const QString &testId, const QMap<QString, QString> &parameters)
{
    qDebug() << "EVENT:" << testId << parameters << "timestamp:" << QDateTime::currentMSecsSinceEpoch();
}
