#include "managers.h"

Managers::Managers(QObject *parent)
    : QObject(parent)
    , m_results(new ResultsManager(this))
    , m_config(new ConfigManager(this))
{
}
