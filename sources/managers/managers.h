#ifndef MANAGERS_H
#define MANAGERS_H

#include <QObject>

#include "results/resultsmanager.h"
#include "config/configmanager.h"
#include "../macros.h"

class Managers : public QObject
{
    Q_OBJECT

    AUTO_Q_PROPERTY(ResultsManager*, results)
    AUTO_Q_PROPERTY(ConfigManager*, config)

public:
    explicit Managers(QObject *parent = nullptr);
};

#endif // MANAGERS_H
