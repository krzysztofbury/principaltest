#ifndef CONFIGMANAGER_H
#define CONFIGMANAGER_H

#include <QObject>
#include <QVector>
#include <QMap>

class ConfigManager : public QObject
{
    Q_OBJECT
public:
    explicit ConfigManager(QObject *parent = nullptr);

public slots:
    void onConfigLoaded(QString configJson);

signals:
    void configUpdated(const QVector<QMap<QString, QString>>& config);

};

#endif // CONFIGMANAGER_H
