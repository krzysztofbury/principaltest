#include <QDebug>

#include "configmanager.h"

ConfigManager::ConfigManager(QObject *parent)
    : QObject(parent)
{
}

void ConfigManager::onConfigLoaded(QString configJson)
{
    QVector<QMap<QString, QString>> vector;

    QMap<QString, QString> presentSelf;
    presentSelf["test_id"] = "class_number_test";
//    vector.append(presentSelf);

    QMap<QString, QString> simpleReaction;
    simpleReaction["test_id"] = "simple_reaction_test";
    simpleReaction["delay_secounds_at_least"] = "1";
    simpleReaction["delay_secounds_maximum"] = "3";
    simpleReaction["number_of_tries"] = "10";
    vector.append(simpleReaction);

    QMap<QString, QString> choiceReaction;
    choiceReaction["test_id"] = "choice_reaction_test";
    choiceReaction["delay_secounds_at_least"] = "1";
    choiceReaction["delay_secounds_maximum"] = "3";
    choiceReaction["number_of_tries"] = "10";
//    vector.append(choiceReaction);

//    two_back_test

    emit configUpdated(vector);
}
