import qbs

Project {
    minimumQbsVersion: "1.7.1"

    CppApplication {
        Depends { name: "Qt.quick" }
        Depends { name: "Qt.core" }
        Depends { name: "cpp" }

        // Additional import path used to resolve QML modules in Qt Creator's code model
        property pathList qmlImportPaths: []

        cpp.cxxLanguageVersion: "c++11"

        cpp.defines: [
            // The following define makes your compiler emit warnings if you use
            // any feature of Qt which as been marked deprecated (the exact warnings
            // depend on your compiler). Please consult the documentation of the
            // deprecated API in order to know how to port your code away from it.
            "QT_DEPRECATED_WARNINGS",

            // You can also make your code fail to compile if you use deprecated APIs.
            // In order to do so, uncomment the following line.
            // You can also select to disable deprecated APIs only up to a certain version of Qt.
            //"QT_DISABLE_DEPRECATED_BEFORE=0x060000" // disables all the APIs deprecated before Qt 6.0.0
        ]

        files: [
            "sources/connector.cpp",
            "sources/connector.h",
            "sources/dataSources/localsource.cpp",
            "sources/dataSources/localsource.h",
            "sources/features/basetest.cpp",
            "sources/features/basetest.h",
            "sources/features/choiceReactionTest/choicereactiontestlogic.cpp",
            "sources/features/choiceReactionTest/choicereactiontestlogic.h",
            "sources/features/classNumberTest/classnumbertestlogic.cpp",
            "sources/features/classNumberTest/classnumbertestlogic.h",
            "sources/features/simpleReactionTest/simplereactiontestlogic.cpp",
            "sources/features/simpleReactionTest/simplereactiontestlogic.h",
            "sources/features/testcontrollerlogic.cpp",
            "sources/features/testcontrollerlogic.h",
            "sources/features/twoBackTest/twobacktestlogic.cpp",
            "sources/features/twoBackTest/twobacktestlogic.h",
            "sources/main.cpp",
            "qml/qml.qrc",
            "sources/managers/config/configmanager.cpp",
            "sources/managers/config/configmanager.h",
            "sources/managers/managers.cpp",
            "sources/managers/managers.h",
            "sources/managers/results/resultsmanager.cpp",
            "sources/managers/results/resultsmanager.h",
            "sources/macros.h",
            "sources/principaltestsapplication.cpp",
            "sources/principaltestsapplication.h",
        ]

        Group {     // Properties for the produced executable
            fileTagsFilter: product.type
            qbs.install: true
        }
    }
}
